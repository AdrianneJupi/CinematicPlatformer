extends Node
class_name animation_player_modifier

#ANIMATION PARSER

func parse_animation(anim_p: AnimationPlayer, operations: Array) -> void:
	var anim = get_anim(anim_p)
	for t in anim.get_track_count():
		var p = str(anim.track_get_path(t))
		for opr in operations:
			var target_track : String = opr["tracks"]
			if target_track == "all" or p.find(target_track) != -1:
				call(opr["method"], anim, t, opr["arg"])


#PARSER FUNCTIONS

func hflip_positions(anim: Animation, track: int, dir: int) -> void:
	for k in anim.track_get_key_count(track):
		var v = anim.track_get_key_value(track, k)
		var av = Vector2(abs(v.x) * sign(dir), v.y)
		anim.track_set_key_value(track, k, av)


func offset_positions(anim: Animation, track: int, offset: Vector2) -> void:
	for k in anim.track_get_key_count(track):
		var v = anim.track_get_key_value(track, k)
		anim.track_set_key_value(track, k, v + offset)


func append_animation(anim: Animation, track: int, args: Array) -> void:
	var target_anim : Animation = args[0]
	var target_paths : Array = args[1]
	var append_time : float = args[2]
	var src_path := anim.track_get_path(track)
	if not target_paths.has(src_path):
		var new_track := target_anim.add_track(anim.track_get_type(track))
		target_anim.track_set_path(new_track, src_path)
		target_paths.append(src_path)
		target_anim.track_set_interpolation_type(
				new_track, anim.track_get_interpolation_type(track))
	for k in anim.track_get_key_count(track):
		var t =  anim.track_get_key_time(track, k) + append_time
		var target_track = target_paths.find(src_path)
		_copy_key(anim, target_anim, track, target_track, k, t)


func _copy_key(anim: Animation, target_anim: Animation, track: int, target_track: int,
		 key: int, time: float) -> void:
	match anim.track_get_type(track):
		Animation.TYPE_VALUE:
			var val = anim.track_get_key_value(track, key)
			target_anim.track_insert_key(target_track, time, val)
		Animation.TYPE_METHOD:
			var name = anim.method_track_get_name(track, key)
			var prms = anim.method_track_get_params(track, key)
			target_anim.track_insert_key(target_track, time, 
					{"method": name, "args": prms})
		Animation.TYPE_AUDIO:
			var eof = anim.audio_track_get_key_end_offset(track, key)
			var sof = anim.audio_track_get_key_start_offset(track, key)
			var strm = anim.audio_track_get_key_stream(track, key)
			# warning-ignore:return_value_discarded
			target_anim.audio_track_insert_key(target_track, time, strm, sof, eof)


func get_end_time(anim: Animation, track: int, res_array: Array) -> void:
	var val : float = res_array[0]
	var last_key := anim.track_get_key_count(track) - 1
	if last_key != -1:
		var key_time := anim.track_get_key_time(track, last_key)
		if key_time > val:
			res_array[0] = key_time


func flip_check(anim: Animation, track: int, res_array: Array) -> void:
	res_array[0] = true if anim.track_get_key_count(track) > 0 else false 


func flip_key_insert(anim: Animation, track: int, args: Array) -> void:
	var dir : int = args[0]
	var flip : bool = args[1]
	var lgth : float = args[2]
	var value := dir == -1
	while anim.track_get_key_count(track) > 0:
		anim.track_remove_key(track, 0)
	anim.track_insert_key(track, 0.0, value)
	if flip:
		anim.track_insert_key(track, lgth, not value)


#OTHER ANIMATION TOOLS

func get_track_paths(anim_p: AnimationPlayer, track_idx := 0) -> Array:
	var paths := []
	var anim := get_anim(anim_p, track_idx)
	for t in anim.get_track_count():
		paths.append(anim.track_get_path(t))
	return paths


func get_anim_end_time(anim_p: AnimationPlayer) -> float:
	var end_time := [0.0]
	parse_animation(anim_p, [{"tracks": "all",
			"method": "get_end_time","arg": end_time}])
	return end_time[0]


func check_anim_flipped(anim_p: AnimationPlayer) -> bool:
	var check := [false]
	parse_animation(anim_p, [{"tracks": "flip_h",
			"method": "flip_check", "arg": check}])
	return check[0]


func get_anim(anim_p: AnimationPlayer, anim_idx:= 0) -> Animation:
	var anims := anim_p.get_animation_list()
	return anim_p.get_animation(anims[anim_idx])


