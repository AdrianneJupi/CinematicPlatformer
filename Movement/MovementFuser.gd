extends Node2D

onready var passed_childrens := owner.get_children()
var passed_player_dir := -1
var anim_player : AnimationPlayer
var tilemap : TileMap
var anim_mod := animation_player_modifier.new()
var col_map_offset := Vector2.ZERO



func _ready():
	_fuse(passed_childrens, passed_player_dir)
	anim_player.play(anim_player.get_animation_list()[0], -1, 1.0)



#GLOBAL
func _fuse(childrens: Array, player_dir: int) -> void:
	var nodes = _acquire_nodes(childrens)
	var animators : Array = nodes["anim_players"]
	var collisions : Array = nodes["coll_paths"]
	anim_player = animators.pop_front()
	
	var length = _setup_anim(anim_player, animators)
	var turn_around = anim_mod.check_anim_flipped(anim_player)
	var effective_dir = player_dir if not turn_around else player_dir * -1
	var flip = false if animators.empty() else turn_around
	
	_set_hflip_keys(anim_player, player_dir, flip, length)
	_fuse_animations(anim_player, animators, length)
	_flip_anim(anim_player, effective_dir)
	#SEND NEW DIRECTION [TURN AROUND] TO PLAYER HERE
	
	if not collisions.empty():
		_fuse_collisions(collisions)
		_orient_tiles(tilemap, effective_dir)



func _acquire_nodes(childrens) -> Dictionary:
	var res = {"anim_players": [], "coll_paths": []}
	for c in childrens:
		for cc in c.get_children():
			if cc is AnimationPlayer:
				res["anim_players"].append(cc)
			elif cc is TileMap:
				res["coll_paths"].append(cc)
	return res



#ANIMATION
func _setup_anim(target: AnimationPlayer, animators: Array) -> float:
	var end_time : float = anim_mod.get_anim_end_time(anim_player)
	var l = end_time if animators.empty() else 60.0
	anim_mod.get_anim(target).length = l
	return end_time


func _fuse_animations(target: AnimationPlayer, animators: Array, lgth: float) -> void:
	if not animators.empty():
		var total_time = lgth
		for animator in animators:
			_transfer_anims(animator, target, total_time)
			total_time += anim_mod.get_anim_end_time(animator)
			animator.owner.queue_free()
		anim_mod.get_anim(target).length = total_time


func _transfer_anims(source: AnimationPlayer, target: AnimationPlayer, time: float) -> void:
	var source_offset : Vector2 = \
			source.owner.global_position - target.owner.global_position
	var tp : Array = anim_mod.get_track_paths(target)
	var ta : Animation = anim_mod.get_anim(target)
	anim_mod.parse_animation(source, [
		{"tracks": "position", "method": "offset_positions", "arg": source_offset},
		{"tracks": "all", "method": "append_animation", "arg": [ta, tp, time]}])


func _set_hflip_keys(target: AnimationPlayer, dir: int, flip: bool, lgth) -> void:
	anim_mod.parse_animation(target, [{"tracks": "flip_h",
			"method": "flip_key_insert", "arg": [dir, flip, lgth]}])


func _flip_anim(target: AnimationPlayer, dir: int) -> void:
	anim_mod.parse_animation(target, [{"tracks": "position",
			"method": "hflip_positions", "arg": dir}])


#TILES
func _fuse_collisions(collisions: Array) -> void:
	assert(collisions.size() > 0)
	tilemap = collisions[0]
	if collisions.size() > 1:
		collisions.pop_front()
		for source in collisions:
			_transfer_tiles(tilemap, source)
			source.owner.queue_free()


func _transfer_tiles(target: TileMap, source: TileMap) -> void:
	var source_positions := source.get_used_cells()
	for cell in source_positions:
		var mofst = (source.global_position - target.global_position)
		var tmpos = cell + (mofst / target.cell_size)
		var tidx = target.get_cellv(tmpos)
		if tidx == TileMap.INVALID_CELL:
			var sidx = source.get_cellv(cell)
			target.set_cellv(tmpos, sidx)


func _orient_tiles(target: TileMap, direction: int) -> void:
	var tile_positions := target.get_used_cells()
	for tile in tile_positions:
		var new_pos = Vector2(abs(tile.x) * sign(direction), tile.y)
		if tile != new_pos:
			target.set_cellv(new_pos, target.get_cellv(tile))
			target.set_cellv(tile, TileMap.INVALID_CELL)
