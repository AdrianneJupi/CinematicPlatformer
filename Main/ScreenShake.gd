extends Node2D


var shaking_active:= false
var strength_low: int
var strength_high: int
var rng = RandomNumberGenerator.new()


func _ready():
	Global.check_error(Effect.connect("screen_shake", self, "on_screen_shake"))


func on_screen_shake(duration: float, strength: float):
	shaking_active = true
	strength_low = - int(floor(strength / 2.0))
	strength_high = int(ceil(strength / 2.0))
	$Duration.start(duration)


func _process(_delta):
	if shaking_active:
		owner.offset = Vector2(
			rng.randi_range(strength_low, strength_high),
			rng.randi_range(strength_low, strength_high)
		)


func _on_Duration_timeout():
	shaking_active = false
	owner.offset = Vector2(0, 0)
	
	
