extends TileMap


func get_tile_at_position(world_position: Vector2) -> String:
	var map_pos = world_to_map(world_position)
	var tile_id = get_cellv(map_pos)
	var tile_name = tile_set.tile_get_name(tile_id) \
			if tile_id != TileMap.INVALID_CELL else "clear"
	return tile_name



