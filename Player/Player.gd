extends Node2D


var current_moveset := "stand"
var forward := ["right", "left"]
var outward := ["right", "left"]
var anim_modifier = preload("res://Movement/AnimationPlayerModifier.gd").new()


func _ready():
	var dir = 1
	anim_modifier.modify_animation($AnimationPlayer, 
			[{"tracks": "position", "method": "hflip_positions", "arg": dir}])
	$Sprite.scale.x = dir
	$AnimationPlayer.play("test")
	get_node("../CollisionMap").get_tile_at_position(global_position)

func _set_forward(fwd: String) -> void:
	assert(["right", "left", "flip"].has(fwd))
	if fwd == "flip":
		forward = _generic_direction(fwd)
	else:
		forward.invert()


func _set_outward(owd: String) -> void:
	assert(["right", "left"].has(owd))
	outward = _generic_direction(owd)


func _generic_direction(fwd: String) -> Array:
	return ["right", "left"] if fwd == "right" else ["left", "right"]
