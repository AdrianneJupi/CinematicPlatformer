extends Node


const MOVESETS := {
	"stand": {
		"step_forward" : "forward",
		"turn_around" : "back",
		"crouch_down" : "down",
		"jump" : "up",
		"run" : ["forward_5"],
		"leap" : ["forward_5", "up"],
		"step_back" : ["back"],
		"hang_down" : ["down"] 
	},
	"crouch": {
		"crouch_forward" : "forward",
		"crouch_turn" : "back",
		"stand" : "up",
		"roll" : ["forward_3"],
		"crouch_back" : ["back"],
		"hang_down" : ["down"] 
	},
	"ledge": {
		"face_outward" : "outward",
		"face_inward" : "inward",
		"drop_down" : "down",
		"climb_up" : "up",
		"ledge_leap" : ["outward", "up"],
		"drop_inside" : ["down", "inward"]
	}
}
