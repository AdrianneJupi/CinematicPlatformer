extends Node

export(NodePath) var player_path : NodePath
export(NodePath) var gui_path : NodePath

const INPUT_DIRECTIONS := ["up", "down", "left", "right"]

onready var player := get_node(player_path)
onready var gui := get_node(gui_path)

var move_data = preload("res://Player/MovesetData.gd").new()

var action_input := false
var active := true setget _set_active

var combos := {"base": [], "direction": [], "side": []}

var valid_combo := ""
var possible_moves := []


#MAIN EVAL FUNCS
func _input(event: InputEvent) -> void:
	if not active:
		return
	if event.is_action_pressed("action"):
		action_input = true
	elif event.is_action_released("action"):
		action_input = false
		if valid_combo != "":
			_use_valid_combo()
	elif event.is_action_pressed("cancel"):
		_clear_combo()
	
	for dr in INPUT_DIRECTIONS:
		if event.is_action_pressed(dr):
			_evaluate_input(dr, move_data.MOVESETS[player.current_moveset])
			_update_gui()
			break


func _use_valid_combo():
	print("COMBO: " +  valid_combo)
	_clear_combo()


func _clear_combo() -> void:
	valid_combo = ""
	for a in combos.values():
		a.clear()
	_update_gui()


func _evaluate_input(input: String, moveset: Dictionary) -> void:
	var translated = _get_translated_input(input)
	if not action_input:
		_evaluate_single(translated, moveset)
	else:
		var combos_copy = combos.duplicate(true)
		var new_combos = _add_to_combo(combos_copy, translated)
		if _evaluate_multiple(new_combos.values(), moveset):
			combos = new_combos.duplicate()


func _evaluate_single(translated_inputs: Array, moveset: Dictionary) -> void:
	for move in moveset:
		if translated_inputs.has(moveset[move]):
			print("SINGLE: ", move)


func _add_to_combo(combos_copy: Dictionary, translated_inputs: Array) -> Dictionary:
	var result := {}
	var names = combos_copy.keys()
	for i in 3:
		var n = names[i]
		var cmb : Array = combos_copy.values()[i]
		var ipt : String = translated_inputs[i]
		if cmb.empty() or (cmb.back().find(ipt) == -1):
			cmb.append(ipt)
		else:
			var last = cmb.size() - 1
			cmb[last] = _increment_input(cmb[last])
		result[n] = cmb
	return result


func _increment_input(input: String) -> String:
	if input.find("_") == -1:
		return input + "_2"
	else:
		var parts := input.split("_")
		return parts[0] + "_" + str(int(parts[1]) + 1)



#COMBO EVALUATION FUNCS
func _evaluate_multiple(new_combos: Array, moveset: Dictionary) -> bool:
	possible_moves.clear()
	var check := false
	for move_name in moveset:
		var move = moveset[move_name]
		if typeof(move) == TYPE_ARRAY and new_combos[0].size() <= move.size():
			for cmb in new_combos:
				var rslt = _check_combo(move_name, move, cmb)
				if rslt != "":
					_add_to_possible_move_list(rslt)
					if move.size() == cmb.size():
						var nb := _get_combo_number(cmb)
						var nm : String = move_name
						valid_combo = nm if nb == 0 else nm + "#" + str(nb)
					check = true
	return check


func _check_combo(m_name: String, move: Array, cmb: Array) -> String:
	var default = ""
	if cmb.empty():
			return default
	for i in cmb.size():
		var mc := false if move[i].find("_") == -1 else true
		var cc := false if cmb[i].find("_") == -1 else true
		if not cc and move[i].find(cmb[i]) == - 1:
			return default
		elif not mc and cc:
			return default
		elif mc and cc and not _check_numbered_inputs(move[i], cmb[i]):
			return default
	return m_name


func _check_numbered_inputs(move: String, cmb: String) -> bool:
	var msplit := move.split("_")
	var csplit := cmb.split("_")
	if msplit[0] != csplit[0]:
		return false
	elif int(msplit[1]) < int(csplit[1]):
		return false
	return true


func _get_combo_number(cmb: Array) -> int:
	for m in cmb:
		if m.to_int() != 0 :
			return m.to_int()
	return 0



#TOOLS
func _add_to_possible_move_list(move: String) -> void:
	if possible_moves.has(move) == false:
		possible_moves.append(move)


func _get_translated_input(input: String) -> Array:
	var translatable := ["right", "left"].has(input)
	var ret = [input] if translatable else [input, input, input]
	if translatable:
		ret.append(_generic_translate(input, player.forward, ["forward", "back"]))
		ret.append(_generic_translate(input, player.outward, ["outward", "inward"]))
	return ret


func _generic_translate(target: String, direction: Array, names: Array) -> String:
	return names[0] if target == direction[0] else names[1]


func _update_gui() -> void:
	gui.update_interface(combos, possible_moves, valid_combo, 
			{"forward": player.forward, "outward": player.outward},
			move_data.MOVESETS[player.current_moveset])


func _set_active(pred: bool) -> void:
	active = pred
	if not active:
		_clear_combo()
