extends Node


static func psa_replace(array: PoolStringArray, find, replace):
	for i in array.size():
		if array[i] == find:
			array[i] = replace
	return array
