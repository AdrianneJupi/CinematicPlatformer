extends Node


signal screen_shake(duration, strength)


func _ready() -> void:
	randomize()


static func screen_freeze(duration: int) -> void:
	OS.delay_msec(duration)
