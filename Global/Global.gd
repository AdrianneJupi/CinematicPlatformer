extends Node


static func check_error(code: int) -> void:
	if code != OK:
		print("ERROR: ", code, " - Check @GlobalScope")
